package com.groove.logic.HitParade;

import java.io.IOException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CookieStore;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.params.ClientPNames;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;

public class HttpHandler {
	private static HttpClient myClient = null;
	private static CookieStore cookieStore = null;
	private static HttpContext localContext = null;
	
	public HttpHandler() {
		if(myClient == null) {
			myClient = new DefaultHttpClient();
			myClient.getParams().setParameter(ClientPNames.ALLOW_CIRCULAR_REDIRECTS, true);
		}
		
		if(cookieStore == null)
			cookieStore = new BasicCookieStore();
		
		if(localContext == null) {
			localContext = new BasicHttpContext();
			localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);
		}
	}
	
	public HttpResponse execute(HttpRequestBase request) {	
		try {
			return myClient.execute(request, localContext);
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
}
