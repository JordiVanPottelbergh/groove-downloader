package com.groove.logic;

public class RequestCreator {
	public static String generateAlbumSongRequest(String albumID, GrooveToken myToken, GrooveSession mySession) {
		String contentToPost = 
				"{" + 
					"\"header\":{" +
						"\"client\":\"" + mySession.getTokenClient() + "\"," +
						"\"clientRevision\":\"" + mySession.getTokenClientRev() + "\"," +
						"\"privacy\":0," +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"," +
						"\"session\":\"" + mySession.getSessionID() + "\"," +
						"\"token\":\"" + myToken.getTokenForRequest("albumGetAllSongs") + "\"" +
					"}," +
					"\"method\":\"albumGetAllSongs\"," +
					"\"parameters\":{" + 
						"\"albumID\":" + albumID +
					"}" +
				"}";
		
		return contentToPost;
	}
	
	public static String generatePopularSongRequest(GrooveToken myToken, GrooveSession mySession) {
		String contentToPost =
				"{" +
					"\"header\":{" +
						"\"client\":\"" + mySession.getTokenClient() + "\"," +
						"\"clientRevision\":\"" + mySession.getTokenClientRev() + "\"," +
						"\"privacy\":0," +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"," +
						"\"session\":\"" + mySession.getSessionID() + "\"," +
						"\"token\":\"" + myToken.getTokenForRequest("popularGetSongs") + "\"" +
					"}," +
					"\"method\":\"popularGetSongs\"," +
					"\"parameters\":{" +
						"\"type\":\"daily\"" +
					"}"+
				"}";
		
		//System.out.println("getStreamKeyFromSongIDEx => " + contentToPost);
		return contentToPost;
	}
	
	public static String generateStreamKeyRequest(int songID, GrooveToken myToken, GrooveSession mySession) {
		String contentToPost =
				"{" +
					"\"method\":\"getStreamKeyFromSongIDEx\"," +
					"\"header\":{" +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"clientRevision\":\"" + mySession.getStreamClientRev() + "\"," +
						"\"privacy\":0," +
						"\"token\":\"" + myToken.getTokenForRequest("getStreamKeyFromSongIDEx") + "\"," +
						"\"session\":\"" + mySession.getSessionID() + "\"," +
						"\"client\":\"" + mySession.getStreamClient() + "\"," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"" +
					"}," +
					
					"\"parameters\":{" +
						"\"songID\":" + songID + "," +
						"\"prefetch\":false," +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"type\":1024," +
						"\"mobile\":false" +
					"}"+
				"}";
		
		//System.out.println("getStreamKeyFromSongIDEx => " + contentToPost);
		return contentToPost;
	}
	
	public static String generateTokenRequest(GrooveSession mySession) {
		String contentToPost =
				"{" +
					"\"method\":\"getCommunicationToken\"," +
					"\"header\":{" +
						"\"session\":\"" + mySession.getSessionID() + "\"," +
						"\"client\":\"" + mySession.getTokenClient() + "\"," +
						"\"clientRevision\":\"" + mySession.getTokenClientRev() + "\"," +
						"\"privacy\":0," + 
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"" +
					"}," +
					"\"parameters\":{" +
						"\"secretKey\":\"" + mySession.getSessionKey() + "\"" +
					"}" +
				"}";
		
		//System.out.println("getCommunicationToken => " + contentToPost);
		return contentToPost;
	}
		
	public static String generateSongSearch(String query, String type, GrooveToken myToken, GrooveSession mySession) {
		String contentToPost = 
				"{" +
					"\"method\":\"getResultsFromSearch\"," +
					"\"header\":{" +
						"\"client\":\"" + mySession.getTokenClient() + "\"," +
						"\"clientRevision\":\"" + mySession.getTokenClientRev() + "\"," +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"," +
						"\"session\":\""+ mySession.getSessionID()  + "\"," +
						"\"token\":\"" + myToken.getTokenForRequest("getResultsFromSearch") + "\"" +
					"}," +
					"\"parameters\":{" +
						"\"query\":\"" +  query + "\"," +
						"\"type\":\"" + type + "\"," +
						"\"guts\":0," +
						"\"ppOverride\":false" +
					"}" +
				"}";
		
		//System.out.println("getResultsFromSearch => " + contentToPost);
		return contentToPost;
	}
	
	public static String generateSongQueuSongPlayed(int songID, int songQueueID, String streamServerID, 
			int songQueueSongID, String streamKey, GrooveSession mySession, GrooveToken myToken) {
		String contentToPost =
				"{" + 
					"\"parameters\":{" +
						"\"songQueueID\":" + songQueueID + "\"," +
						"\"songID\":" + songID + "," +
						"\"streamServerID\":\"" + streamServerID + "\"," + 
						"\"songQueueSongID\":" + songQueueSongID + "," +
						"\"streamKey\":\"" + streamKey + "\"" + 
					"}," +
					"\"header\":{" +
						"\"session\":\""+ mySession.getSessionID()  + "\"," +
						"\"token\":\"" + myToken.getTokenForRequest("markSongQueueSongPlayed") + "\"," +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"privacy\":0," + 
						"\"client\":\"" + mySession.getTokenClient() + "\"," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"," +
						"\"clientRevision\":\"" + mySession.getTokenClientRev() + "\"" +
					"}" +
					"\"method\":\"markSongQueueSongPlayed\"" +
				"}";
		return contentToPost;
	}
	
	public static String generateSongDownloadComplete(GrooveSession mySession, GrooveToken myToken, Song me) {
		String contentToPost = 
				"{" +
					"\"method\":\"markSongDownloadedEx\"," +
					"\"header\":{" +
						"\"country\":" + mySession.getCountryJSON() + "," +
						"\"clientRevision\":\"" + mySession.getTokenClientRev() + "\"," +
						"\"privacy\":0" +
						"\"token\":\"" + myToken.getTokenForRequest("markSongDownloadedEx") + "\"," +
						"\"session\":\""+ mySession.getSessionID()  + "\"," +
						"\"client\":\"" + mySession.getTokenClient() + "\"," +
						"\"uuid\":\"" + mySession.getSessionUUID() + "\"" +
					"}," +
					"\"parameters\":{" +
						"\"streamServerID\":\"" + me.getMyStreamServer().getIp() + "\"," +
						"\"songID\":" + me.getSongID() + "," +
						"\"streamKey\":\"" + me.getMyStreamServer().getStreamKey() + "\"" +
					"}" +
				"}";
		
		//System.out.println("markSongDownlaodedEx => " + contentToPost);
		return contentToPost;
	}
}
