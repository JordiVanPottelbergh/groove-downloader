package com.groove.logic;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;

;

/*
 Example JSON query:

 {
 "header":{
 "session":"116692ae2c8cc4e73e0c4b7deb1b57f6",
 "serviceVersion":"20100903",
 "prefetchEnabled":true
 },

 "result":"512e61759b509"
 }
 */

public class GrooveToken implements Parcelable {
	private boolean DEBUG = false;

	private String session;
	private int serviceVersion;
	private boolean prefetchEnabled;
	private String token;
	
	private String secretKey = "tastyTacos";

	// TEST STRING FOR OFFLINE TESTINGPURPOSE:
	// {\"header\":{\"session\":\"7ca1ddfa690af20b2cefdc1f4a349209\",\"serviceVersion\":\"20100903\",\"prefetchEnabled\":true},\"result\":\"5130b9c9cabe4\"}
	public GrooveToken(GrooveSession mySession) {
		String html;

		if (!DEBUG)
			html = RequestHandler
					.send("https://cowbell.grooveshark.com/more.php?getCommunicationToken",
							RequestCreator.generateTokenRequest(mySession));
		else
			html = "{\"header\":{\"session\":\"7ca1ddfa690af20b2cefdc1f4a349209\",\"serviceVersion\":\"20100903\",\"prefetchEnabled\":true},\"result\":\"5130b9c9cabe4\"}";
				
		try {
			JSONObject json = new JSONObject(html);

			JSONObject header = new JSONObject(json.get("header").toString());

			token = json.getString("result");

			session = header.getString("session");
			serviceVersion = header.getInt("serviceVersion");
			prefetchEnabled = header.getBoolean("prefetchEnabled");
		} catch (JSONException e) {
			System.out.println("no internet :o");
			e.printStackTrace();
		}
	}

	public GrooveToken(Parcel in) {
		session = in.readString();
		serviceVersion = in.readInt();
		prefetchEnabled = (in.readInt() == 1) ? true : false;
		token = in.readString();
		secretKey = in.readString();
	}

	public String getTokenForRequest(String requestMethod) {
		String generatedToken = "";

		String salt = Utils.generateRandomSalt();

		generatedToken = salt
				+ Utils.encodeSha1(requestMethod + ":" + token + ":"
						+ secretKey + ":" + salt);

		return generatedToken;
	}

	public String getSession() {
		return session;
	}

	public int getServiceVersion() {
		return serviceVersion;
	}

	public boolean isPrefetchEnabled() {
		return prefetchEnabled;
	}

	public String getToken() {
		return token;
	}

	public void setSession(String session) {
		this.session = session;
	}

	public void setServiceVersion(int serviceVersion) {
		this.serviceVersion = serviceVersion;
	}

	public void setPrefetchEnabled(boolean prefetchEnabled) {
		this.prefetchEnabled = prefetchEnabled;
	}

	public void setToken(String token) {
		this.token = token;
	}

	@Override
	public String toString() {
		return "Session => " + getSession() + "\n" + "ServiceVersion => "
				+ getServiceVersion() + "\n" + "PrefetchEnabled => "
				+ isPrefetchEnabled() + "\n" + "Token => " + getToken();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeString(session);
		out.writeInt(serviceVersion);
		out.writeInt(prefetchEnabled ? 1 : 0);
		out.writeString(token);
		out.writeString(secretKey);
	}
	
	public static final Parcelable.Creator<GrooveToken> CREATOR = new Parcelable.Creator<GrooveToken>() {
		public GrooveToken createFromParcel(Parcel in) {
			return new GrooveToken(in);
		}

		public GrooveToken[] newArray(int size) {
			return new GrooveToken[size];
		}
	};

}
