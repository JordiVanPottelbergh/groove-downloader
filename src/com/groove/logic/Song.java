package com.groove.logic;

import java.util.HashSet;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import com.groove.dlder.DownloadQueueManager;
import com.groove.dlder.MainActivity;

public class Song implements Parcelable, Comparable<Song>{
	private String format = "[TITLE] - [ARTIST]";
	private String saveName;
	
	private int songID = -1337;
	private int albumID;
	private int artistID;
	private int genreID = -1;
	private String songName;
	private String albumName;
	private String artistName;
	private int year = 0;
	private int trackNum;
	private String covertArtFilename;
	private String artistCoverArtFilename;
	private long tsAdded; //TS MEANS TIMESTAMP, DIDNT KNOW THAT TBH o.O
	private String avgRating;
	private String avgDuration;
	private String estimateDuration;
	private int flags;
	private boolean isLowBitrateAvailable;
	private boolean isVerified;
	private int popularity;
	private double score;
	private int rawScore;
	private int popularityIndex;
	
	//Album stuff
	private boolean isAlbum;
	private boolean isAlbumSong;
	
	private int limit = 20;
	//END OF Album Stuff
	
	private JSONObject me;
	
	private ParsedStreamServer myStreamServer;
	
	private int totalLength = -1;
	private int currentLength = -1;
	private boolean parseSpecial;
	
	private volatile boolean notified = true;
	
	public HashSet<Song> getAllSongsInAlbum() {
		if(isAlbum) {
			String contentJSON = RequestHandler.send("http://cowbell.grooveshark.com/more.php?albumGetAllSongs", 
				RequestCreator.generateAlbumSongRequest(albumID + "", MainActivity.getMyToken(), 
					MainActivity.getMySession()));
			
			try {
				JSONObject json = new JSONObject(contentJSON);
				//JSONObject header = (JSONObject) JSONSerializer.toJSON(json.get("header"));
				
				HashSet<Song> songList = new HashSet<Song>();
				
				JSONArray unparsedList = json.getJSONArray("result");
				
				int maxCount;
				if(unparsedList.length() > limit)
					maxCount = limit;
				else
					maxCount = unparsedList.length();
				
				for(int i = 0; i < maxCount; i++) {
					JSONObject thisSong = (JSONObject) unparsedList.get(i);
					
					Song me = new Song(thisSong, false, true, true);
					
					if(me.isAlbumSong && !me.isVerified())
						continue;
					
					boolean addMe = true;
					
					for(Song cur : songList)
						if(cur.getSongName().toLowerCase(Locale.ENGLISH).equals(me.getSongName().toLowerCase(Locale.ENGLISH)) && addMe)
							addMe = false;
					
					//System.out.println(me.getSongName() + " => is verified: " + me.isVerified());
					
					//TODO: add album verified settings/button
					if(addMe && me.isVerified())
						songList.add(me);
				}
				
				return songList;
				
			} catch (JSONException e) {
				System.out.println("banned in song");
				e.printStackTrace();
			}	
		}
		return null; 
	}
	
	public boolean download(String savePath) {
		if(isAlbum) {
			DownloadQueueManager.download(getAllSongsInAlbum());
		} else {
			myStreamServer = new ParsedStreamServer(
					RequestHandler.send("http://cowbell.grooveshark.com/more.php?getStreamKeyFromSongIDEx", 
				RequestCreator.generateStreamKeyRequest(songID, MainActivity.getMyToken(), MainActivity.getMySession()))
			);

			if (myStreamServer.getIp().equals("FAULT")) {
				Log.v("test", "Got a fault");
				MainActivity.refreshTokens(this);
				this.notified = false;
			}
			
			while (!notified) {
				Log.v("test", "Refreshing token, waiting for notify");
				try {
					Thread.sleep(100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}

				myStreamServer = new ParsedStreamServer(
						RequestHandler
								.send("http://cowbell.grooveshark.com/more.php?getStreamKeyFromSongIDEx",
										RequestCreator.generateStreamKeyRequest(
												songID,
												MainActivity.getMyToken(),
												MainActivity.getMySession())));
			}
			Log.v("test", "End of Notifie, i'm happy");
			
			RequestHandler.send("http://cowbell.grooveshark.com/more.php?markSongDownloadedEx", 
					RequestCreator.generateSongDownloadComplete(MainActivity.getMySession(), MainActivity.getMyToken(), this));
			
			RequestHandler.fetchSong("http://" + myStreamServer.getIp(), myStreamServer.getStreamKey(), savePath, this);
		}
		
		return true;
	}
	
	public Song(JSONObject thisSong, boolean isAlbum, boolean isAlbumSong, boolean parseSpecial) {
		me = thisSong;
		this.setAlbum(isAlbum);
		this.isAlbumSong = isAlbumSong;
		this.parseSpecial = parseSpecial;
		
		try {
			if(!isAlbum)
				songID = thisSong.getInt("SongID");
			
			if(isAlbumSong) {
				isVerified = (thisSong.getString("IsVerified").equals("1") ? true : false);
				trackNum = thisSong.getInt("TrackNum");
			}
				
			if(!parseSpecial) {
				albumID = thisSong.getInt("AlbumID");
				genreID = thisSong.getInt("GenreID");
				
				if(!thisSong.getString("Year").equals(""))
					year = thisSong.getInt("Year");
				
				songName = thisSong.getString("SongName");
				
				tsAdded = thisSong.getLong("TSAdded");
				avgRating = thisSong.getString("AvgRating");
				avgDuration = thisSong.getString("AvgDuration");
				score = thisSong.getDouble("Score");
				rawScore = thisSong.getInt("RawScore");
				popularityIndex = thisSong.getInt("PopularityIndex");
				artistCoverArtFilename = thisSong.getString("ArtistCoverArtFilename");
				
			} else {
				songName = thisSong.getString("Name");
			}

			artistID = thisSong.getInt("ArtistID");
			
			albumName = thisSong.getString("AlbumName");
			artistName = thisSong.getString("ArtistName");
			
			covertArtFilename = thisSong.getString("CoverArtFilename");
			estimateDuration = thisSong.getString("EstimateDuration");
			flags = thisSong.getInt("Flags");
			isLowBitrateAvailable = (thisSong.getString("IsLowBitrateAvailable").equals("1") ? true : false);
			popularity = thisSong.getInt("Popularity");
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		//System.out.println("ARTISTNAME " + artistName);
		
		artistName = artistName.replace('$', 's');
		artistName = artistName.replace('\\', ' ');
		artistName = artistName.replace("’", "'");
		
		songName = songName.replace('$', 's');
		songName = songName.replace('\\', ' ');
		songName = songName.replace("’", "'");
				
		saveName = format + ".mp3";
		saveName = saveName.replaceFirst("\\[TITLE\\]", songName);
		saveName = saveName.replaceFirst("\\[ARTIST\\]", artistName);
	}

	public Song(Parcel in) {
		me = null;
		
		songID = in.readInt();
		albumID = in.readInt();
		artistID = in.readInt();
		genreID = in.readInt();
		songName = in.readString();
		albumName = in.readString();
		artistName = in.readString();
		
		year = in.readInt();
		
		trackNum = in.readInt();
		covertArtFilename = in.readString();
		artistCoverArtFilename = in.readString();
		tsAdded = in.readLong();
		avgRating = in.readString();
		avgDuration = in.readString();
		estimateDuration = in.readString();
		flags = in.readInt();
		isLowBitrateAvailable = (in.readInt() == 1 ? true : false);
		isVerified = (in.readInt() == 1 ? true : false);
		popularity = in.readInt();
		score = in.readDouble();
		rawScore = in.readInt();
		popularityIndex = in.readInt();
		
		isAlbum = (in.readInt() == 1 ? true : false);
		
		artistName = artistName.replace('$', 's');
				
		saveName = format + ".mp3";
		saveName = saveName.replaceFirst("\\[TITLE\\]", songName);
		saveName = saveName.replaceFirst("\\[ARTIST\\]", artistName);
	}

	public int getSongID() {
		return songID;
	}

	public void setSongID(int songID) {
		this.songID = songID;
	}

	public int getAlbumID() {
		return albumID;
	}

	public void setAlbumID(int albumID) {
		this.albumID = albumID;
	}

	public int getArtistID() {
		return artistID;
	}

	public void setArtistID(int artistID) {
		this.artistID = artistID;
	}

	public int getGenreID() {
		return genreID;
	}

	public void setGenreID(int genreID) {
		this.genreID = genreID;
	}

	public String getSongName() {
		return songName;
	}

	public void setSongName(String songName) {
		this.songName = songName;
	}

	public String getAlbumName() {
		return albumName;
	}

	public void setAlbumName(String albumName) {
		this.albumName = albumName;
	}

	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public int getYear() {
		return year;
	}

	public void setYear(int year) {
		this.year = year;
	}

	public int getTrackNum() {
		return trackNum;
	}

	public void setTrackNum(int trackNum) {
		this.trackNum = trackNum;
	}

	public String getCovertArtFilename() {
		return covertArtFilename;
	}

	public void setCovertArtFilename(String covertArtFilename) {
		this.covertArtFilename = covertArtFilename;
	}

	public String getArtistCoverArtFilename() {
		return artistCoverArtFilename;
	}

	public void setArtistCoverArtFilename(String artistCoverArtFilename) {
		this.artistCoverArtFilename = artistCoverArtFilename;
	}

	public long getTsAdded() {
		return tsAdded;
	}

	public void setTsAdded(long tsAdded) {
		this.tsAdded = tsAdded;
	}

	public String getAvgRating() {
		return avgRating;
	}

	public void setAvgRating(String avgRating) {
		this.avgRating = avgRating;
	}

	public String getAvgDuration() {
		return avgDuration;
	}

	public void setAvgDuration(String avgDuration) {
		this.avgDuration = avgDuration;
	}

	public String getEstimateDuration() {
		return estimateDuration;
	}

	public void setEstimateDuration(String estimateDuration) {
		this.estimateDuration = estimateDuration;
	}

	public int getFlags() {
		return flags;
	}

	public void setFlags(int flags) {
		this.flags = flags;
	}

	public boolean isLowBitrateAvailable() {
		return isLowBitrateAvailable;
	}

	public void setLowBitrateAvailable(boolean isLowBitrateAvailable) {
		this.isLowBitrateAvailable = isLowBitrateAvailable;
	}

	public boolean isVerified() {
		return isVerified;
	}

	public void setVerified(boolean isVerified) {
		this.isVerified = isVerified;
	}

	public int getPopularity() {
		return popularity;
	}

	public void setPopularity(int popularity) {
		this.popularity = popularity;
	}

	public double getScore() {
		return score;
	}

	public void setScore(double score) {
		this.score = score;
	}

	public int getRawScore() {
		return rawScore;
	}

	public void setRawScore(int rawScore) {
		this.rawScore = rawScore;
	}

	public int getPopularityIndex() {
		return popularityIndex;
	}

	public void setPopularityIndex(int popularityIndex) {
		this.popularityIndex = popularityIndex;
	}

	public JSONObject getMe() {
		return me;
	}

	public void setMe(JSONObject me) {
		this.me = me;
	}

	@Override
	public String toString() {
		return "Song [songID=" + songID + ", albumID=" + albumID
				+ ", artistID=" + artistID + ", genreID=" + genreID
				+ ", songName=" + songName + ", albumName=" + albumName
				+ ", artistName=" + artistName + ", year=" + year
				+ ", trackNum=" + trackNum + ", covertArtFilename="
				+ covertArtFilename + ", artistCoverArtFilename="
				+ artistCoverArtFilename + ", tsAdded=" + tsAdded
				+ ", avgRating=" + avgRating + ", avgDuration=" + avgDuration
				+ ", estimateDuration=" + estimateDuration + ", flags=" + flags
				+ ", isLowBitrateAvailable=" + isLowBitrateAvailable
				+ ", isVerified=" + isVerified + ", popularity=" + popularity
				+ ", score=" + score + ", rawScore=" + rawScore
				+ ", popularityIndex=" + popularityIndex + "]";
	}

	public String getSaveName() {
		return saveName;
	}

	public void setSaveName(String saveName) {
		this.saveName = saveName;
	}

	public ParsedStreamServer getMyStreamServer() {
		return myStreamServer;
	}

	public void setMyStreamServer(ParsedStreamServer myStreamServer) {
		this.myStreamServer = myStreamServer;
	}
	
	public int getTotalLength() {
		return totalLength;
	}
	
	public void setTotalLength(int totalLength) {
		this.totalLength = totalLength;
	}
	
	public int getCurrentLength() {
		return currentLength;
	}
	
	public void setCurrentLength(int currentLength) {
		this.currentLength = currentLength;
	}
	
	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel out, int flags) {
		out.writeInt(songID);
		out.writeInt(albumID);
		out.writeInt(artistID);
		out.writeInt(genreID);
		out.writeString(songName);
		out.writeString(albumName);
		out.writeString(artistName);
		out.writeInt(year);
		out.writeInt(trackNum);
		out.writeString(covertArtFilename);
		out.writeString(artistCoverArtFilename);
		out.writeLong(tsAdded);
		out.writeString(avgRating);
		out.writeString(avgDuration);
		out.writeString(estimateDuration);
		out.writeInt(flags);
		out.writeInt(isLowBitrateAvailable ? 1 : 0);
		out.writeInt(isVerified ? 1 : 0);
		out.writeInt(popularity);
		out.writeDouble(score);
		out.writeInt(rawScore);
		out.writeInt(popularityIndex);
		out.writeInt(isAlbum ? 1 : 0);
	}
	
	public boolean isAlbum() {
		return isAlbum;
	}

	public void setAlbum(boolean isAlbum) {
		this.isAlbum = isAlbum;
	}

	public boolean isAlbumSong() {
		return isAlbumSong;
	}

	public void setAlbumSong(boolean isAlbumSong) {
		this.isAlbumSong = isAlbumSong;
	}

	public boolean isParseSpecial() {
		return parseSpecial;
	}

	public void setParseSpecial(boolean parseSpecial) {
		this.parseSpecial = parseSpecial;
	}

	public static final Parcelable.Creator<Song> CREATOR = new Parcelable.Creator<Song>() {
		public Song createFromParcel(Parcel in) {
			return new Song(in);
		}

		public Song[] newArray(int size) {
			return new Song[size];
		}
	};
	
	@Override
	public int compareTo(Song another) {
		String myName;
		String hisName;
		
		if (!this.isAlbum()) {
			if (this.isAlbumSong())
				myName = ((this.getTrackNum() < 10 ? "" + 0 + this.getTrackNum() : this.getTrackNum()) + " - " + this.getSongName());
			else
				myName = (this.getSongName());

		} else {
			myName = (this.getAlbumName());
		}
		
		if (!another.isAlbum()) {
			if (another.isAlbumSong())
				hisName = ((another.getTrackNum() < 10 ? "" + 0 + another.getTrackNum() : another.getTrackNum()) + " - " + another.getSongName());
			else
				hisName = (another.getSongName());

		} else {
			hisName = (another.getAlbumName());
		}
		
		return myName.compareTo(hisName);
	}

	public void setNotified(boolean notified) {
		this.notified = notified;
	}

}