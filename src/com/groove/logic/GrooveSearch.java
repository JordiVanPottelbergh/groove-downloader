package com.groove.logic;

import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.groove.dlder.Settings;

import android.app.NotificationManager;
import android.content.Context;

public class GrooveSearch {
	private boolean DEBUG = false;
	
	private String version;
	private String assignedVersion;
	private boolean askForSuggestion;
	private ArrayList<Song> songList;
	
	private String packageName;
	private Context baseContext;
	private NotificationManager mNotificationManager;
		
	private String contentJSON;

	//TEST STRING FOR OFFLINE TESTINGPURPOSE:
	//"{\"header\":{\"session\":\"68063a5a93f941d42fd16f72c7c74adf\",\"serviceVersion\":\"20100903\",\"prefetchEnabled\":true},\"result\":{\"result\":[{\"SongID\":\"24797364\",\"AlbumID\":\"3735648\",\"ArtistID\":\"14650\",\"GenreID\":\"0\",\"SongName\":\"Crash Test\",\"AlbumName\":\"Mechanize\",\"ArtistName\":\"Fear Factory\",\"Year\":\"2010\",\"TrackNum\":\"11\",\"CoverArtFilename\":\"3735648.jpg\",\"ArtistCoverArtFilename\":\"\",\"TSAdded\":\"1268166297\",\"AvgRating\":\"0.000000\",\"AvgDuration\":\"0.000000\",\"EstimateDuration\":\"220.000000\",\"Flags\":0,\"IsLowBitrateAvailable\":\"1\",\"IsVerified\":\"1\",\"Popularity\":1305700033,\"Score\":77865.847490205,\"RawScore\":0,\"PopularityIndex\":33},{\"SongID\":\"3737437\",\"AlbumID\":\"488558\",\"ArtistID\":\"52015\",\"GenreID\":\"0\",\"SongName\":\"Layerd Laird\",\"AlbumName\":\"The Litmus Test\",\"ArtistName\":\"Cut Chemist\",\"Year\":\"2004\",\"TrackNum\":\"12\",\"CoverArtFilename\":\"488558.jpg\",\"ArtistCoverArtFilename\":\"\",\"TSAdded\":\"1192145213\",\"AvgRating\":\"0.000000\",\"AvgDuration\":\"43.000000\",\"EstimateDuration\":\"42.000000\",\"Flags\":0,\"IsLowBitrateAvailable\":\"1\",\"IsVerified\":\"1\",\"Popularity\":1305600033,\"Score\":72825.147473503,\"RawScore\":0,\"PopularityIndex\":33}],\"version\":\"HTP4PopArtist\",\"assignedVersion\":\"HTP4PopArtist\",\"askForSuggestion\":false}}"
	public GrooveSearch(GrooveToken myToken, GrooveSession mySession, String query, String type) {
		if(!DEBUG)
			contentJSON = RequestHandler.send("http://cowbell.grooveshark.com/more.php?getSearchResults",
				RequestCreator.generateSongSearch(query, type, myToken, mySession));
		else
			contentJSON = "{\"header\":{\"session\":\"68063a5a93f941d42fd16f72c7c74adf\",\"serviceVersion\":\"20100903\",\"prefetchEnabled\":true},\"result\":{\"result\":[{\"SongID\":\"24797364\",\"AlbumID\":\"3735648\",\"ArtistID\":\"14650\",\"GenreID\":\"0\",\"SongName\":\"Crash Test\",\"AlbumName\":\"Mechanize\",\"ArtistName\":\"Fear Factory\",\"Year\":\"2010\",\"TrackNum\":\"11\",\"CoverArtFilename\":\"3735648.jpg\",\"ArtistCoverArtFilename\":\"\",\"TSAdded\":\"1268166297\",\"AvgRating\":\"0.000000\",\"AvgDuration\":\"0.000000\",\"EstimateDuration\":\"220.000000\",\"Flags\":0,\"IsLowBitrateAvailable\":\"1\",\"IsVerified\":\"1\",\"Popularity\":1305700033,\"Score\":77865.847490205,\"RawScore\":0,\"PopularityIndex\":33},{\"SongID\":\"3737437\",\"AlbumID\":\"488558\",\"ArtistID\":\"52015\",\"GenreID\":\"0\",\"SongName\":\"Layerd Laird\",\"AlbumName\":\"The Litmus Test\",\"ArtistName\":\"Cut Chemist\",\"Year\":\"2004\",\"TrackNum\":\"12\",\"CoverArtFilename\":\"488558.jpg\",\"ArtistCoverArtFilename\":\"\",\"TSAdded\":\"1192145213\",\"AvgRating\":\"0.000000\",\"AvgDuration\":\"43.000000\",\"EstimateDuration\":\"42.000000\",\"Flags\":0,\"IsLowBitrateAvailable\":\"1\",\"IsVerified\":\"1\",\"Popularity\":1305600033,\"Score\":72825.147473503,\"RawScore\":0,\"PopularityIndex\":33}],\"version\":\"HTP4PopArtist\",\"assignedVersion\":\"HTP4PopArtist\",\"askForSuggestion\":false}}";
						
		try {
			JSONObject json = new JSONObject(contentJSON);
			//JSONObject header = (JSONObject) JSONSerializer.toJSON(json.get("header"));
			
			JSONObject result = new JSONObject(json.get("result").toString());
			if(!type.equals("Albums")) {			
				version = result.getString("version");
				assignedVersion = result.getString("assignedVersion");
				askForSuggestion = result.getBoolean("askForSuggestion");
			}
			
			songList = new ArrayList<Song>();
			
			JSONArray unparsedList = result.getJSONArray("result");
					
			int maxCount;
			if(unparsedList.length() > Settings.getShowSongCount())
				maxCount = Settings.getShowSongCount();
			else
				maxCount = unparsedList.length();
			
			for(int i = 0; i < maxCount; i++) {
				JSONObject thisSong = (JSONObject) unparsedList.get(i);
				
				Song me;
				
				if(type.equals("Albums"))
					me = new Song(thisSong, true, false, false);
				else
					me = new Song(thisSong, false, false, false);
				
				songList.add(me);
			}
		} catch (JSONException e) {
			System.out.println("Banned in groovesearch");
			e.printStackTrace();
		}
	}

	public String getVersion() {
		return version;
	}

	public void setVersion(String version) {
		this.version = version;
	}

	public String getAssignedVersion() {
		return assignedVersion;
	}

	public void setAssignedVersion(String assignedVersion) {
		this.assignedVersion = assignedVersion;
	}

	public boolean getAskForSuggestion() {
		return askForSuggestion;
	}

	public void setAskForSuggestion(boolean askForSuggestion) {
		this.askForSuggestion = askForSuggestion;
	}

	public int getSearchCount() {
		return songList.size();
	}
	
	public Song getSongByName(String songName) {
		for(Song me : songList) {
			if(me.getSongName().equals(songName))
				return me;
		}
		
		return null;
	}
	
	public ArrayList<Song> getSongList() {
		return songList;
	}

	public void setSongList(ArrayList<Song> songList) {
		this.songList = songList;
	}

	public String getContentJSON() {
		return contentJSON;
	}

	public void setContentJSON(String contentJSON) {
		this.contentJSON = contentJSON;
	}
	
	public String getPackageName() {
		return packageName;
	}

	public void setPackageName(String packageName) {
		this.packageName = packageName;
	}

	public Context getBaseContext() {
		return baseContext;
	}

	public void setBaseContext(Context baseContext) {
		this.baseContext = baseContext;
	}

	public NotificationManager getmNotificationManager() {
		return mNotificationManager;
	}

	public void setmNotificationManager(NotificationManager mNotificationManager) {
		this.mNotificationManager = mNotificationManager;
	}
}
