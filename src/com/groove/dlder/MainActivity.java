package com.groove.dlder;

import java.util.ArrayList;
import java.util.HashSet;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.NetworkInfo.State;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.groove.logic.GroovePopular;
import com.groove.logic.GrooveSearch;
import com.groove.logic.GrooveSession;
import com.groove.logic.GrooveToken;
import com.groove.logic.Song;

//TODO LIST:
/*
 * Add view others in album
 */

public class MainActivity extends Activity {
	private EditText etSearch;

	private Button btnSearch;
	private Button btnLeegmaken;
	private Button btnDownloadAll;

	private Spinner spnSearchType;

	private ListView lvSonglist;
	private SongAdapter songAdapter;
	private volatile static ArrayList<Song> songs;

	private volatile static HashSet<Song> toDownload;

	private Song currentSong;

	private static GrooveSession mySession;
	private static GrooveToken myToken = null;

	private ProgressBar mProgress;

	private static SharedPreferences prefs = null;
	private static ConnectivityManager conMan = null;
	private static String packageName = "";
	private static Context baseContext = null;

	private static NotificationManager mNotificationManager;

	public boolean hasWifi() {
		State wifi = conMan.getNetworkInfo(1).getState();
		if (wifi == NetworkInfo.State.CONNECTED)
			return true;

		return false;
	}

	public boolean has3g() {
		State mobile = conMan.getNetworkInfo(0).getState();
		if (mobile == NetworkInfo.State.CONNECTED)
			return true;

		return false;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		
		PreferenceManager.setDefaultValues(this, R.xml.pref_general, false);
		
		/** Fetching & hooking all Views **/
		etSearch = (EditText) findViewById(R.id.txtSongtitel);
		btnSearch = (Button) findViewById(R.id.btnSearch);
		btnLeegmaken = (Button) findViewById(R.id.btnEmpty);
		btnDownloadAll = (Button) findViewById(R.id.btnDownloadAll);
		
		spnSearchType = (Spinner) findViewById(R.id.spnSearchType);
		lvSonglist = (ListView) findViewById(R.id.lvHitParadeSong);
		
		conMan = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		//Settings.setPrefs(getSharedPreferences("com.groove.dlder", Context.MODE_PRIVATE));
        Settings.setPrefs(PreferenceManager.getDefaultSharedPreferences(getBaseContext()));
		mNotificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
		packageName = getPackageName();
		baseContext = getBaseContext();
		
		// initialising the lists if necessary
		if (songs == null)
			songs = new ArrayList<Song>();
		
		if (toDownload == null)
			toDownload = new HashSet<Song>();
		/** END Fetching & hooking all Views **/
		
		/** Initiate all static variable we will need during the progress **/
		
		/**DEBUGGING STUFF COMES HERE**/
		
		/**END OF DEBUGGING**/
		
		// Creating new session and getting new token
		mySession = new GrooveSession();
		new Thread(new Runnable() {
			@Override
			public void run() {
				myToken = new GrooveToken(mySession);
			}
		}).start();
		/** END Initiate all static variable we will need during the progress **/
		
		System.out.println(" DERP " + hasWifi());


		/** Add all the onclicklisteners **/
		btnDownloadAll.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(getBaseContext(), "Downloading Selected Songs", 
						Toast.LENGTH_LONG).show();
				
				DownloadQueueManager.download(toDownload);

				clearDownloadsChecked();
			}
		});
		
		etSearch.setOnKeyListener(new View.OnKeyListener() {

			@Override
			public boolean onKey(View v, int keyCode, KeyEvent event) {
				if (event.getAction() == KeyEvent.ACTION_DOWN) {
					switch (keyCode) {
					case KeyEvent.KEYCODE_DPAD_CENTER:
					case KeyEvent.KEYCODE_ENTER:
						searchSong();
						return true;
					default:
						break;
					}
				}
				return false;
			}
		});

		btnSearch.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				songs.clear();
				songAdapter.notifyDataSetChanged();
				songAdapter.clear();

				clearDownloadsChecked();

				searchSong();
			}
		});

		btnLeegmaken.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				etSearch.setText("");
				songs.clear();
				songAdapter.notifyDataSetChanged();
				songAdapter.clear();

				clearDownloadsChecked();
			}
		});

		lvSonglist.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {

				closeKeyboard();
				Song me = songAdapter.getItem(position);

				boolean checkedStatus = songAdapter.getCheckStatus(position);

				if (checkedStatus) {
					if (toDownload.contains(me))
						toDownload.remove(me);

					songAdapter.setCheckStatus(position, false);
				} else {
					songAdapter.setCheckStatus(position, true);
					toDownload.add(me);
				}

				lvSonglist.invalidateViews();
			}

		});

		// contextmenu, this registers the list for contextmenu (longclick)
		registerForContextMenu(lvSonglist);
		lvSonglist.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
					@Override
					public boolean onItemLongClick(AdapterView<?> parent,
							View view, int position, long id) {
						// this sets the current song to the song which is long
						// clicked
						currentSong = songAdapter.getItem(position);
						
						// this means the longclick isn't consumed and the
						// contextmenu
						// gets called
						return false;
					}
				});

		// Initialising the dropdownlist
		ArrayAdapter<CharSequence> spinnerAdapter = ArrayAdapter
				.createFromResource(this, R.array.SearchTypes,
						android.R.layout.simple_spinner_item);
		spinnerAdapter
				.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spnSearchType.setAdapter(spinnerAdapter);

		mProgress = (ProgressBar) findViewById(R.id.progressBar1);
		mProgress.setVisibility(View.INVISIBLE);// Setting the loadbar to invisible
		
		// making new adapter and putting it on the listview
		songAdapter = new SongAdapter(this, R.layout.song_item,
				R.id.txtListItemArtist, songs);
		lvSonglist.setAdapter(songAdapter);
		/** END Add all the onclicklisteners **/
	}

	public static void refreshTokens(final Song me) {
		mySession = new GrooveSession();
		new Thread(new Runnable() {
			@Override
			public void run() {
				myToken = new GrooveToken(mySession);
				me.setNotified(true);
			}
		}).start();
	}
	
	private void searchSong() {
		closeKeyboard();

		mProgress.setVisibility(View.VISIBLE);

		new Thread(new Runnable() {
			@Override
			public void run() {
				if (etSearch.getText().toString().equals(""))
					return;

				if (myToken == null)
					return;

				GrooveSearch mySearch = null;

				if (((String) spnSearchType.getSelectedItem()).equals("Albums")) {
					mySearch = new GrooveSearch(
							myToken,
							mySession,
							etSearch.getText().toString(),
							(String) spnSearchType.getSelectedItem());
				} else {
					mySearch = new GrooveSearch(myToken, mySession, 
							etSearch.getText().toString(), 
							(String) spnSearchType.getSelectedItem());
				}

				songs = mySearch.getSongList();

				runOnUiThread(new Runnable() {
					public void run() {
						songAdapter.clear();
						
						for (Song me : songs)
							songAdapter.add(me);

						mProgress.setVisibility(View.INVISIBLE);
					}
				});
			}
		}).start();
	}
	
	protected void clearDownloadsChecked() {
		for (int x = 0; x < songs.size(); x++) {
			songAdapter.setCheckStatus(x, false);
		}

		toDownload = new HashSet<Song>();

		lvSonglist.invalidateViews();
	}

	@Override
	protected void onResume() {
		super.onResume();
		currentSong = null;
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// this is called when you hold an item and inflates the menu
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.song_menu, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
			case R.id.download: 
				Toast.makeText(getBaseContext(), "Downloading Song", 
						Toast.LENGTH_LONG).show();
				
				HashSet<Song> hack = new HashSet<Song>();
				hack.add(currentSong);
				
				DownloadQueueManager.download(hack);
				
				clearDownloadsChecked();
				return true;
				
			case R.id.details:
				if(currentSong.isAlbum()) {
					Intent intent = new Intent(getBaseContext(), AlbumDetailActivity.class);
					intent.putExtra("song", currentSong);
					
					startActivity(intent);
				} else {
					Intent intent = new Intent(getBaseContext(), DetailActivity.class);
					intent.putExtra("song", currentSong);
	
					startActivity(intent);
				}
				return true;
			default:
				return super.onContextItemSelected(item);

		}
	}

	public void closeKeyboard() {
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(etSearch.getWindowToken(), 0);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_settings:
			Intent intent = new Intent(getBaseContext(), PrefsActivity.class);
			startActivity(intent);
			break;
		case R.id.PopularSongs:
			clearDownloadsChecked();
			
			new Thread(new Runnable() {
				
				@Override
				public void run() {
					GroovePopular popular = new GroovePopular(myToken, mySession);
					songs = popular.getSongList();

					runOnUiThread(new Runnable() {
						public void run() {
							songAdapter.clear();
							
							for (Song me : songs)
								songAdapter.add(me);

							mProgress.setVisibility(View.INVISIBLE);
						}
					});
				}
			}).start();
			break;
		}

		return super.onOptionsItemSelected(item);
	}
	
	public static NotificationManager getNotificationManager() {
		return mNotificationManager;
	}

	public static SharedPreferences getPrefs() {
		return prefs;
	}

	public static ConnectivityManager getConMan() {
		return conMan;
	}

	public static GrooveSession getMySession() {
		return mySession;
	}

	public static GrooveToken getMyToken() {
		return myToken;
	}
	
	public static String getMyPackageName() {
		return packageName;
	}
	
	public static Context getMyBaseContext() {
		return baseContext;
	}
}
