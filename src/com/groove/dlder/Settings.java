package com.groove.dlder;

import android.content.SharedPreferences;
import android.os.Environment;

public class Settings {
	private static SharedPreferences prefs = null;
	
	public static String getLocation() {
		return prefs.getString("pref_location", 
				Environment.getExternalStorageDirectory().getAbsolutePath() + "/Download/");
	}
	
	public static boolean use3g() {
		return prefs.getBoolean("example_checkbox", false);
	}
	
	public static int getThrottle() {
		return prefs.getInt("pref_throttle", 5)*1000;
	}
	
	public static int getShowSongCount() {
		int retMe = Integer.parseInt(prefs.getString("pref_showSongCount", "20"));
		System.out.println(retMe + " => " + use3g());
		if(retMe < 1)
			retMe = 1000000;
		
		return retMe;
	}
	
	public static void setPrefs(SharedPreferences prefs) {
		Settings.prefs = prefs;
	}

	
}